<?php
/*
 * Cortos de Joneame
 * Idea original: Aritz <aritz@itxaropena.org>
 * Desarrollo: Aritz <aritz@itxaropena.org>
 * 			   Jon <arano.jon@gmail.com>
 * 
 * Clase de cortos
 * Web: http://joneame.net
 */
require_once 'user.php';

class Corto {
var $id;
var $texto;
var $por;
var $carisma;
var $votos;
var $aux;
var $avatar;

	// Get random short message
	function get_random() {
		global $db;

		$cortos_especiales = $this->special_message_active();
		if ($this->id)
			$eskaria = $db->get_row("SELECT * FROM cortos WHERE id = $this->id LIMIT 1");
		elseif ($cortos_especiales)
			$eskaria = $db->get_row("SELECT * FROM cortos WHERE por = ".$cortos_especiales." AND activado = '1' ORDER BY RAND() LIMIT 1");
		

		else
		$eskaria = $db->get_row("SELECT texto,por,id,carisma,votos FROM cortos WHERE activado = '1'  ORDER BY RAND() LIMIT 1");

		$this->texto = $eskaria->texto;
		
		$this->carisma = $eskaria->carisma;
		$this->votos = $eskaria->votos;
		$this->id = $eskaria->id;
		$this->ediciones = $eskaria->ediciones;
		$this->id_autor = $eskaria->por;
		
	    $erab = new User();
		$erab->id = $eskaria->por;

		if ($erab->read()) {
			$this->por = $erab->username;
			if ($erab->avatar != 0) $this->avatar = $erab->avatar; else $this->avatar = 0;
		} else {
			$this->por = "Desconocido";
			$this->avatar = 0;
		}

		$this->aux = $cortos_especiales; 
		

		if ($eskaria)
			return true;
		else
			return false;
	}

	// Get single short message
	function get_single() {
		global $db;

		$eskaria = $db->get_row("SELECT * FROM cortos WHERE id = $this->id and activado = '1' LIMIT 1");

		if (!$eskaria)
			return false;

		$this->texto = $eskaria->texto;
		$this->carisma = $eskaria->carisma;
		$this->votos = $eskaria->votos;
		$this->id = $eskaria->id;
		$this->ediciones = $eskaria->ediciones;
		$this->id_autor = $eskaria->por;
		
		$erab = new User();
		$erab->id = $eskaria->por;

		if ($erab->read()) {
			$this->por = $erab->username;
			if ($erab->avatar != 0) $this->avatar = $erab->avatar; else $this->avatar = 0;
		} else {
			$this->por = "Desconocido";
			$this->avatar = 0;
		}
	}

	function special_message_active() {
		global $db;

		$jaso = $db->get_row("SELECT * FROM gconfig WHERE id = '1'");

		if ($jaso->activo == 1)
		return $jaso->user_id;

		return false;
	}
	
	function change_special_message($status, $uid) {
		global $db;
		
		if (is_int($status) && is_int($uid)) {
			if ($status) // activate
				 $db->query("UPDATE gconfig SET activo = 1, user_id = '".$db->escape($uid)."' WHERE id = 1");
			else
				 $db->query("UPDATE gconfig SET activo = 0 WHERE id = 1");
				 
			return true;
		
		} 
		
		return false;
	}

	// me gustan las chapuzas de jon, oh si.
	function vote_exists() {
		global $current_user;
		require_once(mnminclude.'votes.php');
		$vote = new Vote;
		$vote->user=$current_user->user_id;
		$vote->type='cortos';
		$vote->link=$this->id;
		$this->voted = $vote->exists();
		if ($this->voted) return $this->voted = true;
		else return $this->voted = false;
	}
	
	// funcion marcada para su eliminacion en la v2.1
	function iconos_votos() {
		global $globals, $current_user;
		if ( $current_user->user_karma > $globals['min_karma_for_comment_votes'] && ! $this->vote_exists()) {  
			echo '<span id="c-votes-'.$this->id.'">';
			echo '<a href="javascript:votar_corto('."$current_user->user_id,$this->id,-1".')" title="'._('voto negativo').'"><img style="background: url(\''.$globals['base_url'].'img/iconos/sprite.png\') -2px -852px no-repeat;" src="'.$globals['base_url'].'img/estructura/pixel.gif" alt="'._('voto negativo').'" width="12" height="8"/></a>&nbsp;';
			echo '<a href="javascript:votar_corto('."$current_user->user_id,$this->id,1".')" title="'._('voto positivo').'"><img style="background: url(\''.$globals['base_url'].'img/iconos/sprite.png\') -2px -836px no-repeat;" src="'.$globals['base_url'].'img/estructura/pixel.gif" alt="'._('voto positivo').'" width="12" height="8"/></a>';
			echo '</span>';
		} else {

		if ($this->voted > 0)
			echo '<img style="background: url(\''.$globals['base_url'].'img/iconos/sprite.png\') -2px -868px no-repeat;" src="'.$globals['base_url'].'img/estructura/pixel.gif" alt="'._('votado positivo').'" title="'._('votado positivo').'" width="12" height="8"/>';

		elseif ($this->voted < 0)
			echo '<img style="background: url(\''.$globals['base_url'].'img/iconos/sprite.png\') -2px -884px no-repeat;" src="'.$globals['base_url'].'img/estructura/pixel.gif" alt="'._('votado negativo').'" title="'._('votado negativo').'" width="12" height="8"/>';
		} 
		
	}

	// funcion marcada para su eliminacion en la v2.1
	function info_votos () {
		global $globals;
		echo '<a href="javascript:modal_from_ajax(\''.$globals['base_url'].'backend/mostrar_votos_cortos.php?id='.$this->id.'\')">';
		echo '<img src="'.$globals['base_url'].'img/estructura/pixel.gif" width="12" height="11" style="background: url(\''.$globals['base_url'].'img/iconos/sprite.png\') -2px -818px no-repeat;" alt="+ info" title="'._('¿quién ha votado?').'"/>';
		echo '</a>&nbsp;';
	}

	
         function numero_ediciones(){
	global $db;

	$ediciones = $db->get_var("SELECT ediciones FROM cortos WHERE id=$this->id ");
	return $ediciones;
	}

	function do_corto() {
		global $globals;

		echo '<div class="visor-cortos">';
		echo '<div class="texto-corto fondo-caja">';
		echo clean_text($this->texto); // no pone smileys por motivos obvios -.-
		echo '</div>';

		echo '<div class="corner">-</div>';

		if ($this->avatar)
			echo '<div class="avatar-usuario"><a href="'.get_user_uri($this->por).'"><img src="'.get_avatar_url($this->id_autor, $this->avatar, 80).'"/></a></div>';
		else
			echo '<div class="avatar-usuario"><a href="'.get_user_uri($this->por).'"><img src="'.$globals['base_url'].'img/v2/no-avatar-80.png"/></a></div>';

		echo '<ul class="barra redondo herramientas">';
		echo '<li><a href="'.$globals['base_url'].'cortos.php" style="background: url(\''.$globals['base_url'].'img/iconos/sprite.png\') 0 -496px no-repeat;">'._('recargar'). '</a></li>';
		echo '<li><a href="'.$globals['base_url'].'nuevo_corto.php" style="background: url(\''.$globals['base_url'].'img/iconos/sprite.png\') 0 0 no-repeat;">'._('enviar nuevo corto'). '</a></li>';
		echo '<li><a href="'.get_corto_uri($this->id).'" style="background: url(\''.$globals['base_url'].'img/iconos/sprite.png\') 0 -208px no-repeat;">'._('permalink'). '</a></li>';
		echo '</ul>';
		echo '<div class="nick-usuario">';
		echo '<div class="meta">';

		if ($current_user->user_id > 0 && $this->id_autor != $current_user->user_id)
			$this->iconos_votos();

		echo ' '._('votos').': <span id="vc-'.$this->id.'">'.$this->votos.'</span>, carisma: <span id="vk-'.$this->id.'">'.$this->carisma.'</span>&nbsp;&nbsp;';

		if ($this->votos > 0)
			$this->info_votos();

		echo '</div>';
		echo '<a href="'.get_user_uri($this->por, 'cortos').'">'.$this->por.'</a></div>';

		echo '</div>';
	}

	function get_relative_individual_permalink() {
		// Permalink of the "comment page"
		global $globals;
		if ($globals['base_corto_url']) {
		return $globals['base_url'] . $globals['base_corto_url'] . $this->id;
		} else {
		return $globals['base_url'] . 'corto.php?id=' . $this->id;
		}
	}
}

// se ha transladado esta informacion a cortos.inc.php

?>