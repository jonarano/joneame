<?php
/*
 * LINK Especial para la queer chat
 * Retoques de rendimiento
 *
 * 2009 Aritz <aritz@itxaropena.org>
 */

class Link {
var $id = 0;
	var $author = -1;
	var $blog = 0;
	var $username = false;
	var $randkey = 0;
	var $karma = 0;
	var $valid = false;
	var $date = false;
	var $sent_date = 0;
	var $published_date = 0;
	var $modified = 0;
	var $url = false;
	var $url_title = '';
	var $encoding = false;
	var $status = 'discard';
	var $type = '';
	var $category = 0;
	var $votes = 0;
	var $anonymous = 0;
	var $votes_avg = 0;
	var $negatives = 0;
	var $title = '';
	var $tags = '';
	var $uri = '';
	var $content = '';
	var $content_type = '';
	var $ip = '';
	var $html = false;
	var $read = false;
	var $voted = false;
	var $banned = false;


    function get_short_permalink() {
        global $globals;
        if ($globals['base_story_url']) {
            return 'http://'.get_server_name().$globals['base_url'].$globals['base_story_url'].'0'.$this->id;
        } else {
            return 'http://'.get_server_name().$this->get_relative_permalink();
        }
    }
	function get_relative_permalink() {
		global $globals;
		if (!empty($this->uri) && !empty($globals['base_story_url']) ) {
			return $globals['base_url'] . $globals['base_story_url'] . $this->uri;
		} else {
			return $globals['base_url'] . 'historia.php?id=' . $this->id;
		}
	}
	function get_permalink() {
		return 'http://'.get_server_name().$this->get_relative_permalink();
	}


}
?>
