<?php

//include_once(mnminclude.'ezdb1-simple.php');
include_once(mnminclude.'ez_sql.php');
include_once(mnminclude.'ez_sql_mysql.php');


global $globals;
$db = new ezSQL_mysql($globals['db_user'], $globals['db_password'], $globals['db_name'], $globals['db_server'], $globals['db_master']);
// we now do "lazy connection.
$db->persistent = $globals['mysql_persistent'];
$db->master_persistent = $globals['mysql_master_persistent'];

// For production servers
$db->hide_errors();

// Cache expiry
$db->cache_timeout = 2; // Note: this is hours

$db->cache_dir = '/mysql_cache';

$db->use_disk_cache = true;

// By wrapping up queries you can ensure that the default
// is NOT to cache unless specified
$db->cache_queries = false;



?>
